package ventana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import java.awt.SystemColor;

/**
 * 
 * @author Maricruz D�az-Calder�n
 * @since 21/12/2017
 *
 */

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/imagenes/carrito.png")));
		setTitle("Carrito de compra");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 599, 490);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegstrese = new JLabel("  1. Datos");
		lblRegstrese.setForeground(Color.DARK_GRAY);
		lblRegstrese.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRegstrese.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/user.png")));
		lblRegstrese.setBackground(Color.ORANGE);
		lblRegstrese.setBounds(28, 32, 133, 32);
		contentPane.add(lblRegstrese);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(30, 84, 72, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(30, 109, 72, 14);
		contentPane.add(lblApellidos);
		
		textField = new JTextField();
		textField.setBounds(108, 75, 115, 20);
		
		
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(108, 106, 184, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblDireccin = new JLabel("Direcci\u00F3n");
		lblDireccin.setBounds(30, 134, 72, 14);
		contentPane.add(lblDireccin);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(108, 131, 184, 20);
		contentPane.add(textField_2);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Alicante", "Barcelona", "C\u00E1ceres", "Denia", "Zaragoza"}));
		comboBox.setToolTipText("");
		comboBox.setBounds(108, 156, 115, 20);
		contentPane.add(comboBox);
		
		JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setBounds(30, 159, 46, 14);
		contentPane.add(lblCiudad);
		
		Button button = new Button("\u00BFYa est\u00E1s registrado?");
		button.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 10));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.setBounds(179, 38, 133, 22);
		contentPane.add(button);
		
		JCheckBoxMenuItem chckbxmntmAceptoPolticaDe = new JCheckBoxMenuItem("Acepto pol\u00EDtica de privacidad");
		chckbxmntmAceptoPolticaDe.setSelected(true);
		chckbxmntmAceptoPolticaDe.setBounds(30, 220, 197, 22);
		contentPane.add(chckbxmntmAceptoPolticaDe);
		
		JLabel lblMtodoDe = new JLabel("  2. M\u00E9todo de pago");
		lblMtodoDe.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/metodo_pago.png")));
		lblMtodoDe.setForeground(Color.DARK_GRAY);
		lblMtodoDe.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMtodoDe.setBackground(Color.ORANGE);
		lblMtodoDe.setBounds(354, 32, 175, 32);
		contentPane.add(lblMtodoDe);
		
		JLabel lblCdigoPostal = new JLabel("C\u00F3digo Postal");
		lblCdigoPostal.setBounds(30, 184, 98, 14);
		contentPane.add(lblCdigoPostal);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(50006, 80, 70000, 1));
		spinner.setBounds(118, 181, 75, 20);
		contentPane.add(spinner);
		
		JRadioButton rdbtnPaypal = new JRadioButton("Transferencia");
		rdbtnPaypal.setBounds(394, 134, 135, 23);
		contentPane.add(rdbtnPaypal);
		
		JRadioButton radioButton = new JRadioButton("Paypal");
		radioButton.setBounds(394, 84, 152, 23);
		contentPane.add(radioButton);
		
		JRadioButton rdbtnTarjetaBancaria = new JRadioButton("Tarjeta bancaria");
		rdbtnTarjetaBancaria.setBounds(394, 109, 135, 23);
		contentPane.add(rdbtnTarjetaBancaria);
		
		JRadioButton rdbtnContrareembolso = new JRadioButton("Contrareembolso");
		rdbtnContrareembolso.setBounds(394, 159, 183, 23);
		contentPane.add(rdbtnContrareembolso);
		
		JLabel lblConfirmacinDe = new JLabel("  3. Confirmaci\u00F3n de pedido\r\n");
		lblConfirmacinDe.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/ok-24.png")));
		lblConfirmacinDe.setForeground(Color.DARK_GRAY);
		lblConfirmacinDe.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblConfirmacinDe.setBackground(Color.ORANGE);
		lblConfirmacinDe.setBounds(28, 274, 251, 32);
		contentPane.add(lblConfirmacinDe);
		
		JLabel lblSubtotal = new JLabel("Subtotal");
		lblSubtotal.setBounds(28, 317, 85, 14);
		contentPane.add(lblSubtotal);
		
		JButton btnRealizarPedido = new JButton("REALIZAR PEDIDO");
		btnRealizarPedido.setForeground(new Color(255, 255, 255));
		btnRealizarPedido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnRealizarPedido.setBackground(new Color(255, 153, 51));
		btnRealizarPedido.setBounds(343, 274, 184, 37);
		contentPane.add(btnRealizarPedido);
		
		textField_3 = new JTextField();
		textField_3.setBounds(83, 315, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JTextPane textPane = new JTextPane();
		textPane.setBorder(new LineBorder(SystemColor.activeCaptionBorder));
		textPane.setBounds(28, 373, 324, 68);
		contentPane.add(textPane);
		
		JLabel lbltieneAlgnMensaje = new JLabel("\u00BFTiene alg\u00FAn mensaje para el servicio de mensajer\u00EDa?");
		lbltieneAlgnMensaje.setBounds(28, 356, 353, 14);
		contentPane.add(lbltieneAlgnMensaje);
	}
}
