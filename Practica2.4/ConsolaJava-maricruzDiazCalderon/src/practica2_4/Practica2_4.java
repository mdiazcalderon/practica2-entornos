package practica2_4;

import java.util.Scanner;

public class Practica2_4 {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);

		System.out.println("Introduce una cantidad en litros");

		double litros = lector.nextDouble();

		System.out.println("Selecciona a que medida quieres convertir la cantidad");
		System.out.println("1: Kilolitros");
		System.out.println("2: Decilitros");
		System.out.println("3: Centilitros");
		System.out.println("4: Mililitros");

		int conversion = lector.nextInt();

		switch (conversion) {
		case 1:
			kilolitros(litros);
			break;
		case 2:
			decilitros(litros);
			break;
		case 3:
			centilitros(litros);
			break;
		case 4:
			mililitros(litros);
			break;
		}

		lector.close();
	}
	
	static void kilolitros(double litros) {
		System.out.println(litros + " litros son " + litros / 1000 + " Litros");
	}
	static void decilitros(double litros) {
		System.out.println(litros + " litros son " + litros * 10 + " Decilitros");
	}
	static void centilitros(double litros) {
		System.out.println(litros + " litros son " + litros * 100 + " Centilitros");
	}
	static void mililitros(double litros) {
		System.out.println(litros + " litros son " + litros * 1000 + " Mililitros");
	}
	
	

}
