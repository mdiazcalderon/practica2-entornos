package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*
		 * Hace falta limpiar el buffer despu�s de meter un dato de tipo int para meter uno de tipo String,
		 * ya que sino en el string, en este caso cadenaLeida, mete el intro.
		 *
		 */
		/*SOLUCI�N: hay que introducir:
		 * input.nextLine();
		 */

		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		input.nextLine();
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
