package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*
		 * Cuando el caracter pasado por el argumento no est� en la cadena, el programa devuelve el valor -1.
		 * Al coger esta posici�n -1  para coger la cadena cadenaFinal, se sale de rango y da error.
		 */
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
				
		if (posicionCaracter>=0){
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		}		
		lector.close();
	}

}
