﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_maricruzDiazCalderon
{
    public class MetodosString
    {
        public static int contarMayusculas(string cadena)
        {
            int contador = 0;

            for (int i = 0; i < cadena.Length; i++)
            {
                if(cadena[i] >= 'A' && cadena[i] <= 'Z')
                {
                    contador++;
                }
            }

            return contador;
        }

        public static int contarMinusculas(string cadena)
        {
            int contador = 0;

            for (int i = 0; i < cadena.Length; i++)
            {
                if (cadena[i] >= 'a' && cadena[i] <= 'z')
                {
                    contador++;
                }
            }

            return contador;
        }

        public static String invertirCadena (String cadena)
        {
            String cadenaInvertida = "";

            for (int i = 0; i < cadena.Length; i++) { 
       
                cadenaInvertida = cadena[i] + cadenaInvertida;
            }

            return cadenaInvertida;
        }

        public static bool compararCadenas(String cadena1, String cadena2)
        {
            bool iguales = cadena1.Equals(cadena2);


            if (iguales)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static  String recortarCadena(String cadena, int caracteres)
        {
            String resultado = cadena.Substring(0, caracteres);

            return resultado;
        }

    }
}
