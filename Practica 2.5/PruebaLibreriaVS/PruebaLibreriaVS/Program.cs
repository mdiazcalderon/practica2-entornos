﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVS_maricruzDiazCalderon;

namespace PruebaLibreriaVS
{
    class Program
    {
        static void Main(string[] args)

        {
            Console.WriteLine(MetodosNumericos.esPrimo(7));

            Console.WriteLine(MetodosNumericos.potencia(2, 3));

            Console.WriteLine(MetodosNumericos.resta(275, 75));

            Console.WriteLine(MetodosNumericos.suma(150,25));

            Console.WriteLine(MetodosNumericos.division(10, 2));

            Console.WriteLine(MetodosString.compararCadenas("perro", "gato"));

            Console.WriteLine(MetodosString.contarMayusculas("JuanPablo"));

            Console.WriteLine(MetodosString.contarMinusculas("Ana"));

            Console.WriteLine(MetodosString.invertirCadena("hola"));

            Console.WriteLine(MetodosString.recortarCadena("28/01/1989", 2));




            Console.ReadKey();
        }
    }
}
