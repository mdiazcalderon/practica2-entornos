package libreria;

public class MetodosNumericos {

	public static int potencia(int basePotencia, int exponente) {
		int resultado = 1;

		for (int i = 0; i < exponente; i++) {
			resultado = resultado * basePotencia;
		}

		return resultado;
	}

	public static int suma(int num1, int num2) {
		int resultado = num1 + num2;

		return resultado;
	}

	public static int resta(int num1, int num2) {
		int resultado = num1 - num2;

		return resultado;
	}

	public static int division(int num1, int num2) {
		int resultado = num1 / num2;

		return resultado;
	}

	public static boolean esPrimo(int numero) {
		int contador = 0;
		for (int i = 1; i <= numero; i++) {
			if (numero % i == 0) {
				contador++;
			}
		}
		if (contador == 2) {
			return true;
		} else {
			return false;
		}

	}

}
