package libreria;

public class MetodosString {

	public static int contarMinusculas(String cadena) {
		int contador = 0;

		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) >= 'a' && cadena.charAt(i) <= 'z') {
				contador++;
			}
		}

		return contador;
	}

	public static int contarMayusculas(String cadena) {
		int contador = 0;

		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) >= 'A' && cadena.charAt(i) <= 'Z') {
				contador++;
			}
		}

		return contador;
	}

	public static String invertirCadena(String cadena) {
		String cadenaInvertida = "";

		for (int i = 0; i < cadena.length(); i++) {

			cadenaInvertida = cadena.charAt(i) + cadenaInvertida;
		}

		return cadenaInvertida;
	}

	public static boolean compararCadenas(String cadena1, String cadena2) {
		boolean iguales = cadena1.equals(cadena2);

		if (iguales) {
			return true;
		} else {
			return false;
		}
	}
	
	 public static  String recortarCadena(String cadena, int caracteres){
         String resultado = cadena.substring(0, caracteres);

         return resultado;
     }
	 
	 public static String[] separarFecha(String fecha){
		 
		 String[] fechaSeparada = fecha.split("/");
		 
		 
		return fechaSeparada;
		 
	 }

 }

