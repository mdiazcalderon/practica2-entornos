package pruebaLibreria;

import libreria.MetodosNumericos;

public class Prueba {

	public static void main(String[] args) {
		
		System.out.println(libreria.MetodosNumericos.division(10, 2));
		System.out.println(libreria.MetodosNumericos.esPrimo(7));
		System.out.println(libreria.MetodosNumericos.potencia(2, 3));
		System.out.println(libreria.MetodosNumericos.resta(125, 25));
		System.out.println(libreria.MetodosNumericos.suma(2, 123));
		
		System.out.println(libreria.MetodosString.compararCadenas("gato", "perro"));
		System.out.println(libreria.MetodosString.contarMayusculas("JuanPablo"));
		System.out.println(libreria.MetodosString.contarMinusculas("Ana"));
		System.out.println(libreria.MetodosString.invertirCadena("hola"));
		System.out.println(libreria.MetodosString.recortarCadena("28/01/1989", 2));
		
		

	}

}
